export default class ApiService {
  postReq = async body => {
    const res = await fetch("https://places-dsn.algolia.net/1/places/query", {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(body)
    }).then(response => {
      return response.json();
    });
    return res;
  };
}
