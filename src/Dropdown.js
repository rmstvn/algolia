import React, { Component } from "react";

export default class Dropdown extends Component {
  render() {
    const popupItem = this.props.countries.filter(
      country => country.ID === this.props.selected
    );

    return (
      <div>
        <ul className="country-list">
          {this.props.countries !== 0 && !this.props.selected
            ? this.props.countries.map(country => (
                <li
                  key={country.ID}
                  className={`item ${
                    this.props.selected === country.ID ? "active" : ""
                  } ${this.props.active === country.ID ? "hover" : ""}`}
                  onClick={() => this.props.onClick(country.ID)}
                  onMouseEnter={() => this.props.onMouseEnter(country.ID)}
                  onMouseLeave={this.props.onMouseLeave}
                  onKeyDown={this.props.onKeyPress}
                >
                  {country.name}
                </li>
              ))
            : null}
        </ul>

        <div className="country__info">
          {popupItem.length !== 0 ? (
            <div className="country__info-wrapper">
              <div className="name">Name {popupItem[0].name} </div>
              <div className="population">
                Population {popupItem[0].population}
              </div>
            </div>
          ) : null}
        </div>

        {this.props.countries.length === 0 ? "No such a country" : null}
      </div>
    );
  }
}
