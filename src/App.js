import React, { Component } from "react";
import "./App.css";
import Api from "./apiService";
import Dropdown from "./Dropdown";

class App extends Component {
  state = {
    selected: null,
    active: null,
    query: "",
    hits: []
  };

  api = new Api();

  getData = e => {
    this.setState(
      {
        query: e.target.value,
        hits: []
      },
      () => {
        this.api
          .postReq({
            query: this.state.query,
            language: "en",
            type: "country"
          })
          .then(res => {
            if (res.query !== "" && this.checkReq(this.state.query) !== -1) {
              res = res.hits.slice(0, 5).map(country => {
                return {
                  name: country.locale_names[0],
                  population: country.population,
                  ID: country.objectID
                };
              });

              this.setState({
                hits: res,
                active: res[0].ID,
                selected: null
              });
            } else {
              this.setState({ hits: [] });
            }
          });
      }
    );
  };

  checkReq = query => {
    const regexp = /(\w)+/gi;
    return query.search(regexp);
  };

  onChangeSelected = id => {
    this.setState({
      selected: id,
      query: ""
    });
  };

  turnOnClass = id => {
    this.setState({
      active: id
    });
  };
  turnOffClass = () => {
    this.setState({
      active: null
    });
  };

  handleKeyPress = e => {
    const key = e.key;
    console.log(key);
    if (key === "Enter") {
      this.setState({
        selected: this.state.active,
        query: ""
      });
    }
    e = e || window.event;

    if (e.keyCode === 27) {
      this.setState({ hits: [], selected: null, active: null, query: "" });
    }
  };

  render() {
    return (
      <div className="App">
        <input
          type="text"
          onChange={e => this.getData(e)}
          value={this.state.query}
          onKeyDown={this.handleKeyPress}
        />

        <Dropdown
          index={this.state.hits.ID}
          countries={this.state.hits}
          selected={this.state.selected}
          active={this.state.active}
          onClick={this.onChangeSelected}
          onMouseEnter={this.turnOnClass}
          onMouseLeave={this.turnOffClass}
          onKeyDown={this.handleKeyPress}
        />
      </div>
    );
  }
}

export default App;
